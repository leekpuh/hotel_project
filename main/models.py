from django.db import models
from django.core.validators import MaxValueValidator

# Create your models here.


class RoomType(models.Model):
    TYPES = (
        ('e', 'Эконом'),
        ('c', 'Комфорт'),
        ('l', 'Люкс'),
    )
    BEDS = (
        ('s', '1 односпальная'),
        ('t', '2 односпальные / 1 двуспальная'),
        ('f', '3 односпальные / 1 односпальная + 1 двуспальная'),
    )
    name = models.CharField(max_length=50, verbose_name='Наименование', default='')
    type = models.CharField(max_length=1, choices=TYPES, verbose_name='Тип номера')
    beds = models.CharField(max_length=1, choices=BEDS, verbose_name='Спальные места')
    cost = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Цена (за 1 ночь)')
    description = models.TextField(max_length=1000, verbose_name='Описание номера')

    def __str__(self):
        return self.name


class Room(models.Model):

    STATUSES = (
        ('f', 'Свободен'),
        ('n', 'Занят'),
        ('b', 'Забронирован'),
    )

    name = models.CharField(max_length=50, verbose_name='Наименование', default='')
    room_number = models.PositiveIntegerField(verbose_name='Номер', unique=True, validators=[MaxValueValidator(999)])
    room_floor = models.PositiveIntegerField(verbose_name='Этаж', validators=[MaxValueValidator(99)])
    status = models.CharField(max_length=1, choices=STATUSES, verbose_name='Статус')
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Guest(models.Model):

    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    patronymic = models.CharField(max_length=50, verbose_name='Отчество', null=True)
    phone = models.PositiveIntegerField(verbose_name='Телефон', validators=[MaxValueValidator(99999999999)])
    email = models.CharField(max_length=50, verbose_name='E-mail')

    def __str__(self):
        return '{} {} {}'.format(self.last_name, self.first_name,
                                 self.patronymic or '')


class Booking(models.Model):
    booking_date = models.DateField(verbose_name='Дата бронирования')
    checkin_date = models.DateField(verbose_name='Дата въезда')
    checkout_date = models.DateField(verbose_name='Дата выезда')
    guest_number = models.PositiveIntegerField(verbose_name='Количество гостей', validators=[MaxValueValidator(3)])
    number_of_nights = models.PositiveIntegerField(verbose_name='Количество ночей', validators=[MaxValueValidator(999)])
    guest = models.ForeignKey(Guest, verbose_name='Гость', on_delete=models.CASCADE)
    room = models.ForeignKey(Room, verbose_name='Номер', on_delete=models.CASCADE)
